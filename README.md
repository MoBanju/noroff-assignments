# Noroff Assignments
Assignments of Fullstack Developer

## Contributor
Mussa Banjai

## Table of Assignments
- [Assignment 1](https://gitlab.com/MoBanju/noroff-assignments/-/tree/main/Assignment%201/RPG%20Characters)
- [Assignment 2](https://gitlab.com/MoBanju/noroff-assignments/-/tree/main/Assignment%202/Asignment_2)
- [Assignment 3](https://gitlab.com/MoBanju/noroff-assignments/-/tree/main/Assignment%203/MovieCharactersAPI)
- [Assignment 4](https://gitlab.com/MoBanju/noroff-assignments/-/tree/main/Assignment%204/KomputerStoreApp)
- [Assignment 5](https://gitlab.com/MoBanju/noroff-assignments/-/tree/main/Assignment%205/SignTranslator)
- [Assignment 6]()
