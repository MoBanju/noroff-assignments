export class Bank
{
    constructor(balance)
    {
        this.balance = balance
        this.salary = 0
        this.loan = 0
    }

    
    getLoan(amount)
    {
        this.loan += amount                     // Money added to the loan
        this.balance += amount                  // Money added to the salary
    }

    work()
    {
        this.salary += 100
    }

    bank()
    {
        if(this.loan > 0)                       // If there is still a loan
        {
            this.loan -= this.salary * 0.1      // 10% of the salary goes to loan
            this.balance += this.salary * 0.9   // Rest goes to the balance
            this.salary -= this.salary
        } else {
            this.balance += this.salary         // Full salary to the bank
            this.salary -= this.salary
        }
    }

    repayLoan()
    {
        if(this.loan > this.salary)             // If the loan is bigger than salary
        {
            this.loan -= this.salary            // Full salary goes to the loan
            this.salary -= this.salary
        } else {
            this.salary -= this.loan            // Full loan is paid
            this.loan -= this.loan

            this.balance += this.salary         // The remainder salary goes to the balance
            this.salary -= this.salary
        }
    }

    buy(price)
    {
        if (this.balance > price)               // If there is enough balance, then buy it
        {
            this.balance -= price
        }
    }
}