# Assignment 4: Komputer Store App

An Store application of Tech. This assignment is about Fundamentals of JavaScript.

## Link
https://mobanju.github.io/Komputer-Store/

## Contributor

- Mussa Banjai


## Description

A web store applications that sells Laptops. The user gets access to his balance account, and he can gain money in two ways:
- **Work**: The user can earn 100kr each time he works, and then he just have to bank it into he bank account.
- **Loan**: It is possible for user to loan the money and pay it later. There is  few conditions:
    - User cannot request a loan bigger than double of his balance.
    - The user must repay the remained loan before asking or another one.
    - User cannot have two loans at the same time.

User has freedom to choose the laptop that he wishes on the Select part of Laptops section. the selected computer will be shown bellow with picture, description and price. There is also a buy button where he user can purchase the selected laptop. Then it will come an alert, depending on whether user has enough funds or not. If the user has enough funds, it will receive an congratulations message for purchase. Else, the user will get notified that there is enough funds for the selected laptop.

## Prototype
![Komputer Store Prototype](prototype.png)
