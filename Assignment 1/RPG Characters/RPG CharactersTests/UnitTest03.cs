using RPG_Characters;
using RPG_Characters.HeroClass;
using RPG_Characters.Items;
using RPG_Characters.InvalidException;

namespace RPG_CharactersTests
{
    public class UnitTest03
    {
        Weapon testAxe = new Weapon("Common Axe", 1, Slot.Weapon, TypeWeapon.Axe,1.1,7);
        Armor testPlateBody = new Armor("Common plate body armor", 1, Slot.Body, TypeArmor.Plate, new PrimaryAttributes(1,0,0));
        Weapon testBow = new Weapon("Common Bow", 1, Slot.Weapon, TypeWeapon.Bow, 0.8, 12);
        Armor testClothedHead = new Armor("Common cloth head armor", 1, Slot.Head, TypeArmor.Cloth, new PrimaryAttributes(0, 0, 5));

        [Fact]
        public void Test03_3_highWeapon()       // 1
        {
            // Arrange
            Warrior warrior = new Warrior();
            testAxe.ItemLevel = 2;
            
            // Act & Assert
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon(testAxe));
        }

        [Fact]
        public void Test03_3_highArmor()       // 2
        {
            // Arrange
            Warrior warrior = new Warrior();
            testPlateBody.ItemLevel = 2;

            // Act & Assert
            Assert.Throws<InvalidArmorException>(() => warrior.EquipArmor(testPlateBody,Slot.Body));
        }

        [Fact]
        public void Test03_3_wrongWeapon()       // 3
        {
            Warrior warrior = new Warrior();

            // Act & Assert
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon(testBow));
        }

        [Fact]
        public void Test03_3_wrongArmor()       // 4
        {
            // Arrange
            Warrior warrior = new Warrior();

            // Act & Assert
            Assert.Throws<InvalidArmorException>(() => warrior.EquipArmor(testClothedHead, Slot.Head));
        }

        [Fact]
        public void Test03_3_WeaponMessageSuccess()       // 5
        {
            // Arrange
            Warrior warrior = new Warrior();
            string expected = "new Weapon equipped!";

            // Act
            string actual = warrior.EquipWeapon(testAxe);


            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Test03_3_ArmorMessageSuccess()       // 6
        {
            // Arrange
            Warrior warrior = new Warrior();
            string expected = "new Armor equipped!";

            // Act
            string actual = warrior.EquipArmor(testPlateBody,Slot.Body);

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Test03_3_DamagenoWeapon()       // 7
        {
            // Arrange
            Warrior warrior = new Warrior();
            double expected = 1.00 * (1.00 + (5.00 / 100.00));

            // Act
            double actual = warrior.HeroDamage();

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Test03_3_DamageWeapon()       // 8
        {
            // Arrange
            Warrior warrior = new Warrior();
            warrior.EquipWeapon(testAxe);
            double expected = (7*1.1) * (1.00 + (5.00 / 100.00));

            // Act
            double actual = warrior.HeroDamage();

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Test03_3_DamageWeaponArmor()       // 9
        {
            // Arrange
            Warrior warrior = new Warrior();
            warrior.EquipWeapon(testAxe);
            warrior.EquipArmor(testPlateBody,Slot.Body);
            double expected = (7 * 1.1) * (1.00 + ((5.00 + 1)/ 100.00));

            // Act
            double actual = warrior.HeroDamage();

            // Assert
            Assert.Equal(expected, actual);
        }


    }
}