using RPG_Characters;
using RPG_Characters.HeroClass;

namespace RPG_CharactersTests
{
    public class UnitTest02
    {        
        [Fact]
        public void Test02_1()       // 1
        {
            // Arrange
            Warrior warrior = new Warrior();
            int expected = 1;
            
            // Act
            int actual = warrior.Level;
            
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Test02_2()       // 2
        {
            // Arrange
            Warrior warrior = new Warrior();
            warrior.LevelUp();
            int expected = 2;

            // Act
            int actual = warrior.Level;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Test02_3_Mage()       // 3
        {
            // Arrange
            Mage mage = new Mage();
            PrimaryAttributes expected = new PrimaryAttributes(1,1,8);

            // Act
            PrimaryAttributes actual = mage.BaseAttributes;

            // Assert
            Assert.Equal(expected.Strength, actual.Strength);
            Assert.Equal(expected.Dexterity, actual.Dexterity);
            Assert.Equal(expected.Intelligence, actual.Intelligence);
        }

        [Fact]
        public void Test02_3_Ranger()       // 3
        {
            // Arrange
            Ranger ranger = new Ranger();
            PrimaryAttributes expected = new PrimaryAttributes(1, 7, 1);

            // Act
            PrimaryAttributes actual = ranger.BaseAttributes;

            // Assert
            Assert.Equal(expected.Strength, actual.Strength);
            Assert.Equal(expected.Dexterity, actual.Dexterity);
            Assert.Equal(expected.Intelligence, actual.Intelligence);
        }

        [Fact]
        public void Test02_3_Rogue()       // 3
        {
            // Arrange
            Rogue rogue = new Rogue();
            PrimaryAttributes expected = new PrimaryAttributes(2, 6, 1);

            // Act
            PrimaryAttributes actual = rogue.BaseAttributes;

            // Assert
            Assert.Equal(expected.Strength, actual.Strength);
            Assert.Equal(expected.Dexterity, actual.Dexterity);
            Assert.Equal(expected.Intelligence, actual.Intelligence);
        }

        [Fact]
        public void Test02_3_Warrior()       // 3
        {
            // Arrange
            Warrior warrior = new Warrior();
            PrimaryAttributes expected = new PrimaryAttributes(5, 2, 1);

            // Act
            PrimaryAttributes actual = warrior.BaseAttributes;

            // Assert
            Assert.Equal(expected.Strength, actual.Strength);
            Assert.Equal(expected.Dexterity, actual.Dexterity);
            Assert.Equal(expected.Intelligence, actual.Intelligence);
        }

        [Fact]
        public void Test02_4_Mage()       // 4
        {
            // Arrange
            Mage mage = new Mage();
            mage.LevelUp();
            PrimaryAttributes expected = new PrimaryAttributes(2, 2, 13);

            // Act
            PrimaryAttributes actual = mage.BaseAttributes;

            // Assert
            Assert.Equal(expected.Strength, actual.Strength);
            Assert.Equal(expected.Dexterity, actual.Dexterity);
            Assert.Equal(expected.Intelligence, actual.Intelligence);
        }

        [Fact]
        public void Test02_4_Ranger()       // 4
        {
            // Arrange
            Ranger ranger = new Ranger();
            ranger.LevelUp();
            PrimaryAttributes expected = new PrimaryAttributes(2, 12, 2);

            // Act
            PrimaryAttributes actual = ranger.BaseAttributes;

            // Assert
            Assert.Equal(expected.Strength, actual.Strength);
            Assert.Equal(expected.Dexterity, actual.Dexterity);
            Assert.Equal(expected.Intelligence, actual.Intelligence);
        }

        [Fact]
        public void Test02_4_Rogue()       // 4
        {
            // Arrange
            Rogue rogue = new Rogue();
            rogue.LevelUp();
            PrimaryAttributes expected = new PrimaryAttributes(3, 10, 2);

            // Act
            PrimaryAttributes actual = rogue.BaseAttributes;

            // Assert
            Assert.Equal(expected.Strength, actual.Strength);
            Assert.Equal(expected.Dexterity, actual.Dexterity);
            Assert.Equal(expected.Intelligence, actual.Intelligence);
        }

        [Fact]
        public void Test02_4_Warrior()       // 4
        {
            // Arrange
            Warrior warrior = new Warrior();
            warrior.LevelUp();
            PrimaryAttributes expected = new PrimaryAttributes(8, 4, 2);

            // Act
            PrimaryAttributes actual = warrior.BaseAttributes;

            // Assert
            Assert.Equal(expected.Strength, actual.Strength);
            Assert.Equal(expected.Dexterity, actual.Dexterity);
            Assert.Equal(expected.Intelligence, actual.Intelligence);
        }

    }
}