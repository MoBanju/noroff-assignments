﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Items
{
    /// <summary>
    /// A class that represent either a weapon or armor.
    /// </summary>
    public abstract class Item
    {
        public string ItemName { get; set; }
        public int ItemLevel { get; set; }

        public Slot ItemSlot { get; set; }

        public Item() { }
        public Item(string itemName, int itemLevel, Slot itemSlot)
        {
            ItemName = itemName;
            ItemLevel = itemLevel;
            ItemSlot = itemSlot;
        }

        /// <summary>
        /// This will give you atributes from Weapon/Armor.
        /// </summary>
        /// <returns>Either Damage per Second from Weapon, or Primary Atributes from Armor</returns>
        public abstract double[] ItemAttributes();
    }
}
