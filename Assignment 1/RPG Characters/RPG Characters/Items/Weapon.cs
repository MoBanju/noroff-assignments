﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Items
{
    public class Weapon : Item
    {
        public TypeWeapon TypeWeapon { get; set; }      // The type of weapoon that is used.
        public double AttackSpeed { get; set; }         // How long does it take to attack.
        public int BaseDamage { get; set; }             // The amount of damage that one attack does.

        public Weapon()
        { }
        public Weapon(string itemName, int itemLevel, Slot itemSlot,TypeWeapon typeWeapon, double attackSpeed, int baseDamage) : base(itemName, itemLevel,itemSlot)
        {
            TypeWeapon = typeWeapon;
            AttackSpeed = attackSpeed;
            BaseDamage = baseDamage;
        }

        public override double[] ItemAttributes()
        {
            return new double[] { (BaseDamage * AttackSpeed) };
        }
    }

    /// <summary>
    /// Weapon types.
    /// </summary>
    public enum TypeWeapon
    {
        Axe,
        Bow,
        Dagger,
        Hammer,
        Staff,
        Sword,
        Wand,
    }
}
