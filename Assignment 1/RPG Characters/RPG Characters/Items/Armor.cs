﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.Items
{
    public class Armor : Item
    {
        public PrimaryAttributes ArmorAttributes { get; set; }      // Attributes for Weapon.

        public TypeArmor TypeArmor { get; set; }                    // The type of weapoon that is used.
        public Armor() { }

        public Armor(string itemName, int itemLevel, Slot itemSlot, TypeArmor typeArmor,PrimaryAttributes armorAttributes) : base(itemName, itemLevel,itemSlot)
        {
            ArmorAttributes = armorAttributes;
            TypeArmor = typeArmor;
        }

        public override double[] ItemAttributes()
        {
            return new double[] { ArmorAttributes.Strength, ArmorAttributes.Intelligence, ArmorAttributes.Dexterity };
        }
    }

    /// <summary>
    /// Armor types.
    /// </summary>
    public enum TypeArmor
    {
        Cloth,
        Leather,
        Mail,
        Plate,
    }
}
