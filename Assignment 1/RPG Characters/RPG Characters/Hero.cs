﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RPG_Characters.Items;
using RPG_Characters.InvalidException;


namespace RPG_Characters
{

    /// <summary>
    /// Abstract class that will be used on 4 classes: Mage, Ranger, Rogue and Warrior.
    /// </summary>
    public abstract class Hero
    {
        public string Name { get; set; }
        public int Level { get; set; } = 1;                                                         // Hero will always start on Level 1
        public PrimaryAttributes BaseAttributes { get; set; } = new PrimaryAttributes();            // These are Hero attributes that are based on Level. Each class will start with different attributes numbers

        public Dictionary<Slot, Item> Equipment { get; set; } = new Dictionary<Slot, Item>();       // Hero have slots, and each slot will fit an item (either a weapon or an armor).

        public List<TypeWeapon> WeaponsAllowed { get; set; } = new List<TypeWeapon>();              // List of allowed weapons will vary on each class

        public List<TypeArmor> ArmorsAllowed { get; set; } = new List<TypeArmor>();                 // List of allowed armors will vary on each class

        public Hero() { }
        public Hero(string name)
        {
            Name = name;
        }


        /// <summary>
        /// It will go through the whole equipped items that are type armor, in order to get attributes from each of them.
        /// </summary>
        /// <returns>Total attributes(Base attributes + attributes from equipped armors)</returns>
        public PrimaryAttributes TotalAttributes()
        {
            PrimaryAttributes totalArmorAttributes = new PrimaryAttributes(0, 0, 0);

            foreach(var item in Equipment)
            {
                if(item.Value.GetType != null && item.Value.GetType() == typeof(Armor) )            // Checks if it is not empty and the type is Armor
                {
                    totalArmorAttributes += ((Armor)item.Value).ArmorAttributes;                    // The attribute is added on the total armor points
                }
            }
            return BaseAttributes + totalArmorAttributes;
        }

        /// <summary>
        /// Increases the level and base attributes according to the class
        /// </summary>
        public abstract void LevelUp();

        /// <summary>
        /// Total damage based on class, weapon and armor equipped
        /// </summary>
        public abstract double HeroDamage();


        /// <summary>
        /// It will go through the whole equipped items to get attributes fro each of them
        /// </summary>
        /// <param name="weapon">The weapon. The weapon will be sloted automtically on weapon slot.</param>
        /// <returns>A successful equipment message</returns>
        /// <exception cref="InvalidWeaponException">
        /// It throws when either the hero didnt reach the level required for weapon, or when the weapon is not on allowed weapons list for specific hero.
        /// </exception>
        public string EquipWeapon(Weapon weapon)
        {
            if(!WeaponsAllowed.Contains(weapon.TypeWeapon))
            {
                throw new InvalidWeaponException($"{weapon.TypeWeapon} {weapon.ItemName} is not allowed for this class. Try another Weapon.");
            }
            else if (Level < weapon.ItemLevel)
            {
                throw new InvalidWeaponException($"{Name} must reach to level {weapon.ItemLevel} in order to obtain the {weapon.TypeWeapon} {weapon.ItemName}.");
            }

            Equipment[Slot.Weapon] = weapon;
            return "new Weapon equipped!";
        }

        /// <summary>
        /// An armor will be placed on specific slot.
        /// </summary>
        /// <param name="armor">The armor</param>
        /// <param name="armorslot">The slot that the armor will be placed</param>
        /// <returns>A successful equipment message</returns>
        /// <exception cref="InvalidArmorException">
        /// It throws when either the hero didnt reach the level required for armor, when the armor is placed at wrong spot, or when the armor is not on allowed weapons list for specific hero.
        /// </exception>
        public string EquipArmor(Armor armor, Slot armorslot)
        {
            if (!ArmorsAllowed.Contains(armor.TypeArmor))
            {
                throw new InvalidArmorException($"{armor.TypeArmor} {armor.ItemName} is not allowed for this class. Try another Armor.");
            }
            else if (Level < armor.ItemLevel)
            {
                throw new InvalidArmorException($"{Name} must reach to level {armor.ItemLevel} in order to obtain the {armor.TypeArmor} {armor.ItemName}.");
            }
            else if(armorslot == Slot.Weapon)
            {
                throw new InvalidArmorException($"{armor.TypeArmor} {armor.ItemName} does not fit on Weapon Slot. Try elsewhere");
            }
            else if (armor.ItemSlot != armorslot)
            {
                throw new InvalidArmorException($"{armor.TypeArmor} {armor.ItemName} is in the wrong slot. You must equip in {armor.ItemSlot}");
            }
            Equipment[armorslot] = armor;
            return "new Armor equipped!";
        }

        /// <summary>
        /// Display all the info about the hero.
        /// </summary>
        public void HeroDisplay()
        {
            Console.WriteLine($"Hero: {Name}");
            Console.WriteLine($"Hero Level: {Level}");
            Console.WriteLine($"Strength: {BaseAttributes.Strength} + {TotalAttributes().Strength - BaseAttributes.Strength}");
            Console.WriteLine($"Dexterity: {BaseAttributes.Dexterity} + {TotalAttributes().Dexterity - BaseAttributes.Dexterity}");
            Console.WriteLine($"Intelligence: {BaseAttributes.Intelligence} + {TotalAttributes().Intelligence - BaseAttributes.Intelligence}");
            Console.WriteLine($"Hero Damage: {HeroDamage()}");
        }
    }

    /// <summary>
    /// Slots for Items
    /// </summary>
    public enum Slot
    {
        Head,
        Body,
        Legs,
        Weapon,
    }
}
