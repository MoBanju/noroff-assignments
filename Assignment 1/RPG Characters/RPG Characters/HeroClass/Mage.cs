﻿using RPG_Characters.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.HeroClass
{
    public class Mage : Hero
    {
        public Mage()
        {
            BaseAttributes.Strength = 1;
            BaseAttributes.Dexterity = 1;
            BaseAttributes.Intelligence = 8;

            WeaponsAllowed.AddRange(new List<TypeWeapon> { TypeWeapon.Staff, TypeWeapon.Wand });
            ArmorsAllowed.AddRange(new List<TypeArmor> { TypeArmor.Cloth });
        }

        public Mage(string name) : base(name)
        {
            BaseAttributes.Strength = 1;
            BaseAttributes.Dexterity = 1;
            BaseAttributes.Intelligence = 8;
            
            WeaponsAllowed.AddRange(new List<TypeWeapon> { TypeWeapon.Staff, TypeWeapon.Wand });
            ArmorsAllowed.AddRange(new List<TypeArmor> { TypeArmor.Cloth });
        }

        public override void LevelUp()
        {
            Level += 1;
            BaseAttributes.Strength += 1;
            BaseAttributes.Dexterity += 1;
            BaseAttributes.Intelligence += 5;

            

        }

        public override double HeroDamage()
        {
            double extra = 0.00;
            foreach (var item in Equipment)
            {
                if (item.Value.GetType != null && item.Value.GetType() == typeof(Armor))
                {
                    extra += ((Armor)item.Value).ArmorAttributes.Intelligence;
                }
            }
            if (Equipment != null)
            { 
                if (Equipment.ContainsKey(Slot.Weapon) || Equipment.GetValueOrDefault(Slot.Weapon) != null)
                {
                    return Convert.ToInt32(Equipment.GetValueOrDefault(Slot.Weapon).ItemAttributes().GetValue(0)) * (1.00 + ((BaseAttributes.Intelligence + extra)/ 100.0));
                }
            }
            return 1 * (1.00 + ((BaseAttributes.Intelligence + extra)/ 100.0));
            

        }
    }
}
