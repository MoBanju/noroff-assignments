﻿using RPG_Characters.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.HeroClass
{
    public class Rogue : Hero
    {
        public Rogue()
        {
            BaseAttributes.Strength = 2;
            BaseAttributes.Dexterity = 6;
            BaseAttributes.Intelligence = 1;

            WeaponsAllowed.AddRange(new List<TypeWeapon> { TypeWeapon.Dagger, TypeWeapon.Sword });
            ArmorsAllowed.AddRange(new List<TypeArmor> { TypeArmor.Leather, TypeArmor.Mail });
        }

        public Rogue(string name) : base(name)
        {
            BaseAttributes.Strength = 2;
            BaseAttributes.Dexterity = 6;
            BaseAttributes.Intelligence = 1;

            WeaponsAllowed.AddRange(new List<TypeWeapon> { TypeWeapon.Dagger, TypeWeapon.Sword });
            ArmorsAllowed.AddRange(new List<TypeArmor> { TypeArmor.Leather, TypeArmor.Mail });
        }

        public override void LevelUp()
        {
            Level += 1;
            BaseAttributes.Strength += 1;
            BaseAttributes.Dexterity += 4;
            BaseAttributes.Intelligence += 1;

        }

        public override double HeroDamage()
        {
            double extra = 0.00;
            foreach (var item in Equipment)
            {
                if (item.Value.GetType != null && item.Value.GetType() == typeof(Armor))
                {
                    extra += ((Armor)item.Value).ArmorAttributes.Dexterity;
                }
            }
            if (Equipment.ContainsKey(Slot.Weapon))
            {
                return Convert.ToInt32(Equipment.GetValueOrDefault(Slot.Weapon).ItemAttributes().GetValue(0)) * (1.00 + ((BaseAttributes.Dexterity + extra) / 100.0));
            }

            return 1 * (1.00 + ((BaseAttributes.Dexterity + extra) / 100.0));

        }
    }
}
