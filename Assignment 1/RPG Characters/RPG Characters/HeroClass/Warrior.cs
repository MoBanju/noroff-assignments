﻿using RPG_Characters.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters.HeroClass
{
    public class Warrior : Hero
    {
        public Warrior()
        {
            BaseAttributes.Strength = 5;
            BaseAttributes.Dexterity = 2;
            BaseAttributes.Intelligence = 1;

            WeaponsAllowed.AddRange(new List<TypeWeapon> { TypeWeapon.Axe, TypeWeapon.Hammer, TypeWeapon.Sword });
            ArmorsAllowed.AddRange(new List<TypeArmor> { TypeArmor.Mail, TypeArmor.Plate });
        }

        public Warrior(string name) : base(name)
        {
            BaseAttributes.Strength = 5;
            BaseAttributes.Dexterity = 2;
            BaseAttributes.Intelligence = 1;

            WeaponsAllowed.AddRange (new List<TypeWeapon> {TypeWeapon.Axe, TypeWeapon.Hammer, TypeWeapon.Sword });
            ArmorsAllowed.AddRange(new List<TypeArmor> { TypeArmor.Mail, TypeArmor.Plate });

        }

        public override void LevelUp()
        {
            Level += 1;
            BaseAttributes.Strength += 3;
            BaseAttributes.Dexterity += 2;
            BaseAttributes.Intelligence += 1;
        }

        public override double HeroDamage()
        {
            double extra = 0.00;
            foreach (var item in Equipment)
            {
                if (item.Value.GetType != null && item.Value.GetType() == typeof(Armor))
                {
                    extra += ((Armor)item.Value).ArmorAttributes.Strength;
                }
            }
            if (Equipment.ContainsKey(Slot.Weapon))
            {
                return Convert.ToDouble(Equipment.GetValueOrDefault(Slot.Weapon).ItemAttributes().GetValue(0)) * (1.00 + ((BaseAttributes.Strength + extra) / 100.0));
            }

            return 1 * (1.00 + ((BaseAttributes.Strength + extra) / 100.0));

        }
    }
}
