﻿using RPG_Characters;
using RPG_Characters.HeroClass;
using RPG_Characters.Items;

CreateGame(); // Testing game


void CreateGame()
{
    Hero ninja = new Mage("Danzo");
    Weapon windy = new Weapon("Windy", 1, Slot.Weapon, TypeWeapon.Wand, 0.5, 150);
    Armor silk = new Armor("Silk", 1, Slot.Body, TypeArmor.Cloth,new PrimaryAttributes(1,1,3));

    ninja.HeroDisplay();
    Console.WriteLine("\n");

    // Level up 4 times
    ninja.LevelUp();
    ninja.LevelUp();
    ninja.LevelUp();
    ninja.LevelUp();
    ninja.HeroDisplay();
    Console.WriteLine("\n");


    // Equipping weapon and arrmor
    ninja.EquipWeapon(windy);
    ninja.EquipArmor(silk, Slot.Body);
    ninja.HeroDisplay();
 
}

