﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Characters
{
    public class PrimaryAttributes
    {
        public int Strength { get; set; } = 0;
        public int Dexterity { get; set; } = 0;
        public int Intelligence { get; set; } = 0; 

        public PrimaryAttributes() { }
        public PrimaryAttributes(int strength, int dexterity, int intelligence)
        {
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }
        public int PrimaryTotalPoints()
        {
            return Strength + Dexterity + Intelligence;
        }

        public static PrimaryAttributes operator +(PrimaryAttributes self, PrimaryAttributes other)
        {
            return new PrimaryAttributes(
                strength: self.Strength + other.Strength,
                dexterity: self.Dexterity + other.Dexterity,
                intelligence: self.Intelligence + other.Intelligence
                );
        }
    }
}
