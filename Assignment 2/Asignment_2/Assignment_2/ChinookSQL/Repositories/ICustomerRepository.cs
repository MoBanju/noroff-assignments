﻿using Assignment_2.ChinookSQL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_2.ChinookSQL.Repositories
{
    ///  <summary>
    /// Helper interface for the CustomerRepository class
    /// </summary>
    public interface ICustomerRepository
    {
        /// <summary>
        /// Searches database for a customer based on ID
        /// </summary>
        /// <param name="id">Id of a customer</param>
        /// <returns>Returns the customer based on Id</returns>
        public Customers GetCustomer(int id);
        
        /// <summary>
        /// Finds all customers based on their Last Name. 
        /// </summary>
        /// <param name="name">Name to search for</param>
        /// <returns>List of customers matching the parameter </returns>
        public List<Customers> GetCustomerByName(string name);

        /// <summary>
        /// Read all the customers from the database and add to a list of customers.
        /// Finds a subset of customers using offset and limit
        /// </summary>
        /// <param name="limit">Amount of rows</param>
        /// <param name="offset">Rows to offset</param>
        /// <returns>List of customers</returns>
        public List<Customers> GetCustomerPage(int limit, int offset);

        /// <summary>
        /// Read all the customers from the database and add to a list of customers.
        /// </summary>
        /// <returns>The list of customers</returns>
        public List<Customers> GetAllCustomers();

        /// <summary>
        /// Add a new customer to the database
        /// </summary>
        /// <param name="customer"> customer object</param>
        /// <returns>Successful if it manages to update the customer</returns>
        public bool AddNewCustomer(Customers customer);
        
        /// <summary>
        /// Updates the customer in the database by customerID
        /// </summary>
        /// <param name="customer">Takes in a customerobject</param>
        /// <returns>Successful if it manages to update the customer</returns>
        public bool UpdateCustomer(Customers customer);

        /// <summary>
        /// Makes a list containing number of customers from each country
        /// </summary>
        /// <returns>List of total customers from each country.</returns>
        public List<CustomerCountry> CountCountry();
        
        /// <summary>
        /// Reads the amount that customers had spent. The list is sorted from the higher amount spent to the fewest.
        /// </summary>
        /// <returns>List of Customers that spends the most from the top.</returns>
        public List<CustomerSpender> HighInvoice();
        
        /// <summary>
        /// Reads the Top(s) Genre(s) played by a specific Customer and ads to the list.
        /// </summary>
        /// <param name="id">customer ID</param>
        /// <returns>Favorite(s) Genre(s) from specific Customer.</returns>
        public List<CustomerGenre> PopularGenre(int id);
    }
}
