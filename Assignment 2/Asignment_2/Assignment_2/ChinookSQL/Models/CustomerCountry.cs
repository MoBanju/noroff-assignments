﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_2.ChinookSQL.Models
{   ///  <summary>
    /// class for country and amount from that country
    /// </summary>
    public class CustomerCountry
    {
        public string Country { get; set; }
        public int Total { get; set; }
    }
}
