﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_2.ChinookSQL.Models
{   /// <summary>
    /// class and properties for customer and their spendings
    /// </summary>
    public class CustomerSpender
    {
        public int CustomerID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public decimal Total { get; set; }
    }
}

