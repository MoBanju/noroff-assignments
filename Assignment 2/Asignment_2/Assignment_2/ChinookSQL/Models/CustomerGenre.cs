﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_2.ChinookSQL.Models
{   ///  <summary>
    /// class and properties for customer genre
    /// </summary>
    public class CustomerGenre
    {
        public string Name { get; set; }    // Name of the Genre
        public int Total { get; set; }
        public int CustomerId { get; set; }
        public string CustomerFirstName { get; set; }
        public string CustomerLastName { get; set; }
    }
}
