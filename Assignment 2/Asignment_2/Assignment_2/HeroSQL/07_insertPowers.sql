﻿INSERT INTO Powerr (NameF, DescriptionF) VALUES ('Fire','Burns');
INSERT INTO Powerr (NameF, DescriptionF) VALUES ('Water', 'Drown');
INSERT INTO Powerr (NameF, DescriptionF) VALUES ('Wind', 'Flies to it');
INSERT INTO Powerr (NameF, DescriptionF) VALUES ('Transformation', 'Transforms with the clock');

INSERT INTO SuperHeroPowers(SuperHeroId, PowerId) VALUES (3,4);
INSERT INTO SuperHeroPowers(SuperHeroId, PowerId) VALUES (1,3);
INSERT INTO SuperHeroPowers(SuperHeroId, PowerId) VALUES (3,3);
INSERT INTO SuperHeroPowers(SuperHeroId, PowerId) VALUES (3,2);
INSERT INTO SuperHeroPowers(SuperHeroId, PowerId) VALUES (2,2);
INSERT INTO SuperHeroPowers(SuperHeroId, PowerId) VALUES (3,1);

SELECT * FROM SuperHero;
SELECT * FROM Assistant;
SELECT * FROM Powerr;
SELECT * FROM SuperHeroPowers;