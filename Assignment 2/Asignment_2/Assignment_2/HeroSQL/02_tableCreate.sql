﻿CREATE TABLE SuperHero(
    SuperHeroId int IDENTITY(1,1),
    NameF varchar(255),
    Alias varchar(255),
    Origin varchar(255),
    PRIMARY KEY (SuperHeroId)
);

CREATE TABLE Assistant(
    AssistantId int IDENTITY(1,1), 
    NameF varchar(255),
    PRIMARY KEY (AssistantId),
);

CREATE TABLE Powerr(
    PowerId int IDENTITY(1,1),
    NameF varchar(255),
    DescriptionF varchar(255),
    PRIMARY KEY (PowerId),
);

SELECT * FROM SuperHero;
SELECT * FROM Assistant;
SELECT * FROM Powerr;