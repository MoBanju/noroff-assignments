﻿--ALTER TABLE SuperHero
--ADD CONSTRAINT PowerId
--FOREIGN KEY(PowerId) REFERENCES Powerr(PowerId);

--ALTER TABLE Powerr
--ADD CONSTRAINT SuperHeroId
--FOREIGN KEY(SuperHeroId) REFERENCES SuperHero(SuperHeroId);


CREATE TABLE SuperHeroPowers(
    SuperHeroId int,
    PowerId int,
    PRIMARY KEY (SuperHeroId,PowerId),
    CONSTRAINT FK_SuperHeroId FOREIGN KEY (SuperHeroId) REFERENCES SuperHero(SuperHeroId) ,
    CONSTRAINT FK_PowerId FOREIGN KEY (PowerId) REFERENCES Powerr(PowerId),
);

SELECT * FROM SuperHero;
SELECT * FROM Assistant;
SELECT * FROM Powerr;