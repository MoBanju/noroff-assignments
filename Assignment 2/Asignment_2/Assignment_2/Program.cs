﻿// See https://aka.ms/new-console-template for more information
using Assignment_2.ChinookSQL.Models;
using Assignment_2.ChinookSQL.Repositories;

//UNCOMMENT TO TEST THE DIFFERENT METHODS

//Ex01();
//Ex02(50);
//Ex03("sen");
//Ex04(5,1);
//Ex05();
//Ex06(5); 
//Ex07();
//Ex08();
//Ex09(12);


void Ex01() // READ ALL CUSTOMERS FROM DATABASE AND DISPLAY
{
    ICustomerRepository repository = new CustomerRepository();
    List<Customers> customers = repository.GetAllCustomers();
    foreach (Customers customer in customers)
    {
        Console.WriteLine($"{customer.CustomerID}\t{customer.FirstName}\t{customer.LastName}\t{customer.Country}\t{customer.PostalCode}\t{customer.PhoneNumber}\t{customer.Email}");
    }
}

void Ex02(int id) //READ SPESIFIC CUSTOMER BY ID
{
    ICustomerRepository repository = new CustomerRepository();
    Customers customer = repository.GetCustomer(id);
    Console.WriteLine($"{customer.CustomerID}\t{customer.FirstName}\t{customer.LastName}\t{customer.Country}\t{customer.PostalCode}\t{customer.PhoneNumber}\t{customer.Email}");
}

void Ex03(string name) //READ CUSTOMER(S) BY LASTNAME
{
    ICustomerRepository repository = new CustomerRepository();
    name = "%" + name + "%";
    List<Customers> customers = repository.GetCustomerByName(name);
    foreach(Customers customer in customers)
    {
        Console.WriteLine($"{customer.CustomerID}\t{customer.FirstName}\t{customer.LastName}\t{customer.Country}\t{customer.PostalCode}\t{customer.PhoneNumber}\t{customer.Email}");
    }
}

void Ex04(int limit, int offset) //DISPLAY SUBSET OF CUSTOMERS
{
    ICustomerRepository repository = new CustomerRepository();
    List<Customers> customers = repository.GetCustomerPage(limit, offset);
    foreach (Customers customer in customers)
    {
        Console.WriteLine($"{customer.CustomerID}\t{customer.FirstName}\t{customer.LastName}\t{customer.Country}\t{customer.PostalCode}\t{customer.PhoneNumber}\t{customer.Email}");
    }
}

void Ex05() //ADD NEW CUSTOMER TO DATABASE
{
    ICustomerRepository repository = new CustomerRepository();
    Customers customer = new Customers()
    {
        FirstName = "Johann",
        LastName = "Olson",
        Country = "Norway",
        PostalCode = "4019",
        PhoneNumber = "97862244",
        Email = "JOlson@hotmail.com",
    };

    Console.WriteLine(repository.AddNewCustomer(customer));
}

void Ex06(int id) //UPDATE EXISTING CUSTOMER
{
    ICustomerRepository repository = new CustomerRepository();
    Customers customer = repository.GetCustomer(id);

    customer.Country = "Japan";
    Console.WriteLine(repository.UpdateCustomer(customer));

}

void Ex07() //RETURN NUMBER OF CUSTOMERS FROM EACH COUNTRY
{
    ICustomerRepository repository = new CustomerRepository();
    List<CustomerCountry> customerCountries  = repository.CountCountry();
    foreach (CustomerCountry country in customerCountries)
    {
        Console.WriteLine($"{country.Country}: {country.Total}");
    }
}

void Ex08() //HIGHEST SPENDING CUSTOMERS
{
    ICustomerRepository repository = new CustomerRepository();
    List<CustomerSpender> customerSpenders = repository.HighInvoice();
    foreach (CustomerSpender spender in customerSpenders)
    {
        Console.WriteLine($"{spender.CustomerID} {spender.FirstName} {spender.LastName}: {spender.Total}");
    }
}

void Ex09(int id) //FOR A GIVEN CUSTOMER, SHOW THEIR MOST POPULAR GENRE
{
    ICustomerRepository repository = new CustomerRepository();
    List<CustomerGenre> customerGenres = repository.PopularGenre(id);
    foreach (CustomerGenre genre in customerGenres)
    {
        Console.WriteLine($" {genre.CustomerId} - {genre.CustomerFirstName} {genre.CustomerLastName};" +
            $"\tGenre: {genre.Name}\tTotal: {genre.Total}");
    }
}
