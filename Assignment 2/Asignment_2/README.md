# Assignment 2: Data access with SQL Client

## Descripion
The project is divided in two Appendixes:
* Appendix A: Super Heros
* Appendix B: Chinoook

### Appendix A
In this appendix, we created a database about SuperHeroes. The goal of this appendix was to write SQL queries to create, read and modify elements in a database. SuperHeroes have Powers and Assistants. Each Assistant is associated to one SuperHero, although one SuperHero can have many Assistants. SuperHeroes can have multiple Powers, and those Powers can be associated with many Heroes. 
The diagram was created in order to have an idea about how our database is going to look.
<details><summary>Diagram</summary>
![Super Hero Database](HerosDiagram.png "Diagram")
</details>



### Appendix B
In this appendix we were given a databased named Chinook. The goal of this appendix was to create a console application in C#, connect to the database to modify, manipulate and extract data from this database. To achieve this we, among other things, implemented the interface below.

<details><summary>Repository Interface</summary>

```
public interface ICustomerRepository
{
    public Customers GetCustomer(int id);
    public List<Customers> GetCustomerByName(string name);
    public List<Customers> GetCustomerPage(int limit, int offset);
    public List<Customers> GetAllCustomers();
    public bool AddNewCustomer(Customers customer);
    public bool UpdateCustomer(Customers customer);
    public bool DeleteCustomer(int id);
    public List<CustomerCountry> CountCountry();
    public List<CustomerSpender> HighInvoice();
    public List<CustomerGenre> PopularGenre(int id);
}
```

</details>
The repository has methods where each method runs either a simple query, for instance select all customers, or more complex queries for returning the number of the customers from each country.


#### Technologies used:
* Visual Studio Community 2022 
* .NET 6
* C# 
* Microsoft SQL Server Management Studio (SSMS)
* SQL queries


#### Packages
Install System.Data.SqlClient


#### Usage
Uncomment lines in program.cs to run the methods


## Contributions
* [Eivind Bertelsen](https://gitlab.com/EivindTB)
* [Mussa Banjai](https://gitlab.com/MoBanju)

