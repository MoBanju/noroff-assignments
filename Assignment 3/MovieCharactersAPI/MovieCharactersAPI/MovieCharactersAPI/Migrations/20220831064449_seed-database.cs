﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MovieCharactersAPI.Migrations
{
    public partial class seeddatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_CharacterMovie",
                table: "CharacterMovie");

            migrationBuilder.DropIndex(
                name: "IX_CharacterMovie_MoviesId",
                table: "CharacterMovie");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CharacterMovie",
                table: "CharacterMovie",
                columns: new[] { "MoviesId", "CharactersId" });

            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "Id", "Alias", "FullName", "Gender", "Picture" },
                values: new object[,]
                {
                    { 1, "Iron Man", "Tony Stark", "Male", null },
                    { 2, "Captein America", "Steve Rogers", "Male", null },
                    { 3, "Black Widow", "Natasha Romanoff", "Female", null },
                    { 4, null, "Thor", "Male", null },
                    { 5, null, "Frodo", "Male", null },
                    { 6, null, "Gandalf", "Male", null },
                    { 7, null, "Pippin", "Male", null },
                    { 8, null, "Galadriel", "Female", null }
                });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "The Marvel Cinematic Universe (MCU) films are a series of American superhero films produced by Marvel Studios based on characters that appear in publications by Marvel Comics. The MCU is the shared universe in which all of the films are set. The films have been in production since 2007, and in that time Marvel Studios has produced and released 29 films, with at least 14 more in various stages of development. It is the highest-grossing film franchise of all time, having grossed over $27.4 billion at the global box office. This includes Avengers: Endgame, which became the highest-grossing film of all time at the time of its release. ", "Marvel Movies" },
                    { 2, "The Lord of the Rings is an epic high-fantasy novel by English author and scholar J. R. R. Tolkien. Set in Middle-earth, intended to be Earth at some time in the distant past, the story began as a sequel to Tolkien's 1937 children's book The Hobbit, but eventually developed into a much larger work. Written in stages between 1937 and 1949, The Lord of the Rings is one of the best-selling books ever written, with over 150 million copies sold. ", "Lord of the Rings franchise" }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "Picture", "ReleaseYear", "Title", "Trailer" },
                values: new object[,]
                {
                    { 1, "Joss Whedon", 1, "Action,Adventure,Sci-Fi", "https://www.imdb.com/title/tt0848228/mediaviewer/rm3955117056/?ref_=tt_ov_i", 2009, "The Avengers", "https://www.imdb.com/video/vi1891149081?playlistId=tt0848228&ref_=tt_ov_vi" },
                    { 2, "Joss Whedon", 1, "Action,Advendture,Sci-Fi", "https://www.imdb.com/title/tt2395427/mediaviewer/rm4050576640/?ref_=tt_ov_i", 2015, "Avengers: Age of Ultron", "https://www.imdb.com/video/vi2821566745?playlistId=tt2395427&ref_=tt_pr_ov_vi" },
                    { 3, "Anthony Russo", 1, "Action,Adventure,Sci-Fi", "https://www.imdb.com/title/tt3498820/mediaviewer/rm3218348288/?ref_=tt_ov_i", 2016, "Captain America: Civil War", "https://www.imdb.com/video/vi174044441?playlistId=tt3498820&ref_=tt_ov_vi" },
                    { 4, "Ryan Coogler", 1, "Action,Adventure,Sci-Fi", "https://www.imdb.com/title/tt1825683/mediaviewer/rm172972800/?ref_=tt_ov_i", 2018, "Black Panther", "https://www.imdb.com/video/vi2320939289?playlistId=tt1825683&ref_=tt_ov_vi" },
                    { 5, "Taika Waititi", 1, "Action,Adventure,Sci-Fi", "https://www.imdb.com/title/tt3501632/mediaviewer/rm1413491712/?ref_=tt_ov_i", 2017, "Thor: Ragnarok", "https://www.imdb.com/video/vi4010391833?playlistId=tt3501632&ref_=tt_ov_vi" },
                    { 6, "Peter Jackson", 2, "Action,Adventure,Drama,Fantasy", "https://www.imdb.com/title/tt0120737/mediaviewer/rm3592958976/?ref_=tt_ov_i", 2001, "The Lord of the Rings: The Fellowship of the Ring", "https://www.imdb.com/video/vi684573465?playlistId=tt0120737&ref_=tt_ov_vi" },
                    { 7, "Peter Jackson", 2, "Action,Adventure,Drama,Fantasy", "https://www.imdb.com/title/tt0167261/mediaviewer/rm306845440/?ref_=tt_ov_i", 2002, "The Lord of the Rings: The Two Towers", "https://www.imdb.com/video/vi701350681?playlistId=tt0167261&ref_=tt_ov_vi" },
                    { 8, "Peter Jackson", 2, "Action,Adventure,Drama,Fantasy", "https://www.imdb.com/title/tt0167260/mediaviewer/rm584928512/?ref_=tt_ov_i", 2003, "The Lord of the Rings: The Return of the King", "https://www.imdb.com/video/vi718127897?playlistId=tt0167260&ref_=tt_ov_vi" }
                });

            migrationBuilder.InsertData(
                table: "CharacterMovie",
                columns: new[] { "CharactersId", "MoviesId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 2, 1 },
                    { 3, 1 },
                    { 4, 1 },
                    { 1, 2 },
                    { 2, 2 },
                    { 3, 2 },
                    { 4, 2 },
                    { 1, 3 },
                    { 2, 3 },
                    { 3, 3 },
                    { 4, 3 },
                    { 4, 4 },
                    { 5, 6 },
                    { 6, 6 },
                    { 7, 6 },
                    { 8, 6 },
                    { 5, 7 },
                    { 6, 7 },
                    { 7, 7 },
                    { 8, 7 },
                    { 5, 8 },
                    { 6, 8 },
                    { 7, 8 },
                    { 8, 8 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_CharacterMovie_CharactersId",
                table: "CharacterMovie",
                column: "CharactersId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_CharacterMovie",
                table: "CharacterMovie");

            migrationBuilder.DropIndex(
                name: "IX_CharacterMovie_CharactersId",
                table: "CharacterMovie");

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 1, 1 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 2, 1 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 3, 1 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 4, 1 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 1, 2 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 2, 2 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 3, 2 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 4, 2 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 1, 3 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 2, 3 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 3, 3 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 4, 3 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 4, 4 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 5, 6 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 6, 6 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 7, 6 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 8, 6 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 5, 7 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 6, 7 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 7, 7 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 8, 7 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 5, 8 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 6, 8 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 7, 8 });

            migrationBuilder.DeleteData(
                table: "CharacterMovie",
                keyColumns: new[] { "CharactersId", "MoviesId" },
                keyValues: new object[] { 8, 8 });

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Franchises",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Franchises",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.AddPrimaryKey(
                name: "PK_CharacterMovie",
                table: "CharacterMovie",
                columns: new[] { "CharactersId", "MoviesId" });

            migrationBuilder.CreateIndex(
                name: "IX_CharacterMovie_MoviesId",
                table: "CharacterMovie",
                column: "MoviesId");
        }
    }
}
