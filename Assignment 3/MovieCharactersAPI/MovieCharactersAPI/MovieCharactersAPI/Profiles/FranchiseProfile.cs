﻿using AutoMapper;
using MovieCharactersAPI.DTOs;
using MovieCharactersAPI.Models;

namespace MovieCharactersAPI.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<FranchiseCreateDto, Franchise>();
            CreateMap<Franchise, FranchiseReadDto>().ReverseMap();
            CreateMap<FranchiseUpdateDto, Franchise>();
        }
    }
}
