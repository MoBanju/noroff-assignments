﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Data;
using MovieCharactersAPI.DTOs;
using MovieCharactersAPI.Interfaces;
using MovieCharactersAPI.Models;

namespace MovieCharactersAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    public class CharactersController : ControllerBase
    {
        private readonly ICharacterRepository _characterRepository;
        private readonly IMapper _mapper;

        public CharactersController( IMapper mapper, ICharacterRepository characterRepository)
        {
            _mapper = mapper;
            _characterRepository = characterRepository;
        }

        // GET: api/Characters
        /// <summary>
        /// Gets all characters.
        /// </summary>
        /// <returns>A list of characters.</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<CharacterReadDto>>> GetCharacters()
        {
            var characters = await _characterRepository.GetAll();
            if (characters is null)
                return NoContent();
            return Ok(characters.Select(character => _mapper.Map<CharacterReadDto>(character)));
        }

        // GET: api/Characters/5
        /// <summary>
        /// Get a specific character by Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A character</returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CharacterReadDto>> GetCharacter(int id)
        {
            var character = await _characterRepository.GetById(id);

            if (character == null)
            {
                return NotFound();
            }

            return _mapper.Map<CharacterReadDto>(character);
        }

        // PUT: api/Characters/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Updates a character using the provided character model and an Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characterDTO"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> PutCharacter(int id, CharacterUpdateDto characterDTO)
        {
            var character = _mapper.Map<Character>(characterDTO);
            if (id != character.Id)
            {
                return BadRequest();
            }

            character = await _characterRepository.Update(character);
            if (character == null)
                return NotFound();
            return NoContent();
        }

        // POST: api/Characters
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        /// <summary>
        /// Post a character, and returns the newly created one.
        /// </summary>
        /// <param name="characterDTO"></param>
        /// <returns>A character</returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<CharacterReadDto>> PostCharacter(CharacterCreateDto characterDTO)
        {
            var character = _mapper.Map<Character>(characterDTO);
            character = await _characterRepository.Add(character);

            return CreatedAtAction("GetCharacter", new { id = character.Id }, _mapper.Map<CharacterReadDto>(character));
            }

        // DELETE: api/Characters/5
        /// <summary>
        /// Deletes an character by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> DeleteCharacter(int id)
        {

            var success = await _characterRepository.Delete(id);
            if(!success)
                return NotFound();

            return NoContent();
        }

        private bool CharacterExists(int id)
        {
            return _characterRepository.Exist(id);
        }
    }
}
