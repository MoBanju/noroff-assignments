using System.ComponentModel.DataAnnotations;

namespace MovieCharactersAPI.DTOs
{
    public class FranchiseUpdateDto
    {
        public int Id { get; set; }
        [StringLength(64)] public string Name { get; set; }
        [StringLength(1024)] public string Description { get; set; }
    }
}