﻿namespace MovieCharactersAPI.Interfaces
{
    public interface IRepository<T>
    {
        /// <summary>
        /// It adds an item on database
        /// </summary>
        /// <param name="item"></param>
        /// <returns>Them item added</returns>
        public Task<T> Add(T item);

        /// <summary>
        /// Gets all the items
        /// </summary>
        /// <returns>All items in the database</returns>
        public Task<List<T>> GetAll();

        /// <summary>
        /// Gets the item based on Id
        /// </summary>
        /// <param name="id">item Id</param>
        /// <returns>item by Id</returns>
        public Task<T?> GetById(int id);

        /// <summary>
        /// Updates the item.
        /// </summary>
        /// <param name="item">The new item with changes</param>
        /// <returns>item updated</returns>
        public Task<T?> Update(T item);

        /// <summary>
        /// Deletes the item
        /// </summary>
        /// <param name="id">item id</param>
        /// <returns>Either the delete ws successful or not</returns>
        public Task<bool> Delete(int id);

        /// <summary>
        /// Check if item exists in Db.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>true if exists</returns>
        public bool Exist(int id);

    }
}
