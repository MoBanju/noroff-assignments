﻿using MovieCharactersAPI.Models;

namespace MovieCharactersAPI.Interfaces
{
    public interface IFranchiseRepository : IRepository<Franchise>
    {
        /// <summary>
        /// Returns all characters participating in a franchise. 
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <returns></returns>
        public Task<ICollection<Character>> GetCharacters(int franchiseId);

        /// <summary>
        /// Returns all movies beloning to a franchise 
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <returns>A list of movies</returns>
        public Task<List<Movie>> GetMovies(int franchiseId);
        /// <summary>
        /// Updates which movies belongs to a franchise.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movieIds"></param>
        /// <returns>The franchise updated</returns>
        public Task<Franchise?> UpdateMovies(int id, int[] movieIds);
    }
}
