using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Data;
using MovieCharactersAPI.Interfaces;
using MovieCharactersAPI.Models;

namespace MovieCharactersAPI.Repositories;

public class MovieRepository : IMovieRepository
{
    private readonly MovieCharactersDbContext _context;

    public MovieRepository(MovieCharactersDbContext context)
    {
        _ = context.Movies ?? throw new ArgumentNullException("context.Movie was null for some reason?");
        _context = context;
    }

    public async Task<Movie> Add(Movie item)
    {
        await _context.Movies.AddAsync(item);
        await _context.SaveChangesAsync();
        return item;
    }

    public async Task<bool> Delete(int id)
    {
        Movie? movie = await GetById(id);
        if(movie is null)
            return false;
        _context.Remove(movie);
        await _context.SaveChangesAsync();
        return true;
    }

    public bool Exist(int id)
    {
        return _context.Movies.Any(m => m.Id == id);
    }

    public async Task<List<Movie>> GetAll()
    {
        return await _context.Movies.ToListAsync();
    }

    public async Task<Movie?> GetById(int id)
    {
        return await _context.Movies.FindAsync(id);
    }

    public async Task<List<Character>> GetCharacters(int id)
    {
            Movie? movie = await _context.Movies.Include(m => m.Characters).FirstOrDefaultAsync(m => m.Id == id);
            if (movie is null)
                return new List<Character>();
            return movie.Characters.ToList();
    }

    public async Task<Movie?> Update(Movie item)
    {
        _context.Entry(item).State = EntityState.Modified;
        await _context.SaveChangesAsync();
        return item;
    }

    public async Task<List<Character>> UpdateCharacters(int movieId, int[] characterIds)
    {
            List<Character> characters = await _context.Characters.Where(c => characterIds.Contains(c.Id)).ToListAsync();

            Movie? movie = await _context.Movies.Include(movie => movie.Characters).FirstOrDefaultAsync(movie => movie.Id == movieId);
            if (movie is null)
                return characters;

            foreach (var character in characters)
                movie.Characters.Add(character);

            await _context.SaveChangesAsync();

            return characters;
    }
}