﻿using Microsoft.EntityFrameworkCore;
using MovieCharactersAPI.Data;
using MovieCharactersAPI.Interfaces;
using MovieCharactersAPI.Models;

namespace MovieCharactersAPI.Repositories
{
    public class CharacterRepository : ICharacterRepository
    {
        private readonly MovieCharactersDbContext _context;

        public CharacterRepository(MovieCharactersDbContext context)
        {
            _ = context.Characters ?? throw new ArgumentNullException("context.Characters was null for some reason?");
            _context = context;
        }

        public async Task<Character> Add(Character item)
        {
            _context.Characters.Add(item);
            await _context.SaveChangesAsync();

            return item;
        }

        public async Task<bool> Delete(int id)
        {
            var character = await GetById(id);//_context.Characters.FindAsync(id);
            if (character == null)
            {
                return false;
            }

            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();

            return true;
        }

        public bool Exist(int id)
        {
            return _context.Characters.Any(c => c.Id == id);
        }

        public Task<List<Character>> GetAll()
        {
            return _context.Characters.ToListAsync();
        }

        public async Task<Character?> GetById(int id)
        {
            return await _context.Characters.FindAsync(id);
        }

        public async Task<Character> Update(Character item)
        {
            _context.Entry(item).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            return item;

        }

    }
}
